maxi = 0
maxiAddress = ''
dictionary = dict()
fname = input('enter file name: ')
try:
    fhand = open(fname)
except:
    print('File cannot be opened:', fname)

for line in fhand:
    words = line.split()
    if len(words) < 3 or words[0] != 'From': continue
    else:
        if words[1] not in dictionary: dictionary[words[1]] = 1

        else: dictionary[words[1]] += 1

for address in dictionary:
    if dictionary[address] > maxi:
        maxi = dictionary[address]
        maxiAddress = address

print(maxi,maxiAddress)
