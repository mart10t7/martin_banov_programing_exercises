dictionary = dict()
fname = input('enter file name: ')
try:
    fhand = open(fname)
except:
    print('File cannot be opened:', fname)

for line in fhand:
    words = line.split()
    if len(words) < 3 or words[0] != 'From': continue
    else:
        aPosition = words[1].find("@")
        domainName = words[1][aPosition+1:]
        if domainName not in dictionary: dictionary[domainName] = 1

        else: dictionary[domainName] += 1

print(dictionary)
