fhand = open('mbox-short.txt')
for line in fhand:
    words = line.split()
    # Since we require the 3rd word in words if there is a line
    # made only by 'From' it will throw an error
    if len(words) < 3: continue

    if words[0] != 'From': continue

    print(words[2])
