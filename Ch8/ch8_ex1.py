def chop(listt):
    del listt[0]
    del listt[-1]

def middle(listt):
    changedList = listt[1:]
    del changedList[-1]
    return changedList

list1 = [1,2,3,4,5]
list2 = [1,2,3,4,5]

choppedList = chop(list1)
print(list1,choppedList)

middleList = middle(list2)
print(list2,middleList)
