import re

revision = []

filename = input('Enter file: ')
fhand = open(filename)

for line in fhand:
    line = line.rstrip()
    revisions = re.findall('^New Revision: ([0-9.]+)', line)
    if revisions:
        for v in revisions:
            v = float(v)
            revision.append(v)

revisionSumm = sum(revision)
count = int(len(revision))
revisionAverage = revisionSumm / count

print(revisionAverage)
