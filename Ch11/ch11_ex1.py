import re

count = 0

inputExpression = input('enter regular expression: ')
regularExpression = str(inputExpression)
filename = input('enter file name :')
fhand = open(filename)

for line in fhand:
    line = line.rstrip()
    if re.findall(regularExpression, line):
        count += 1

print(filename,' had ',str(count),' lines that matched ',regularExpression)
