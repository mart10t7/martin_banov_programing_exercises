fname = input('Enter the file name: ')
count = 0
number = 0
try:
    fhand = open(fname)
except:
    print('File cannot be opened:', fname)
    exit()
for line in fhand:
    if line.startswith("X-DSPAM-Confidence:"):
        position = line.find(":")
        number += float(line[position+2:])
        count += 1
average = number/count
print(average)