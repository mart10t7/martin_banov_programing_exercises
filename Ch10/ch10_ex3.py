import string

countLet = 0
dictionary = dict()
relList = list()

filename = input('enter file name: ')
fhand = open(filename)

for line in fhand:
    line = line.translate(str.maketrans('', '', string.digits))
    line = line.translate(str.maketrans('', '', string.punctuation))
    line = line.lower()
    words = line.split()
    for word in words:
        for letter in word:
            countLet += 1
            if letter not in dictionary:
                dictionary[letter] = 1
            else:
                dictionary[letter] += 1

for k,v in list(dictionary.items()):
    relList.append((v/countLet,k))

relList.sort(reverse=True)

for k,v in relList:
    print(k,v)
