list1 = list()
dictionary = dict()
filename = input('enter file name: ')
try:
    fhand = open(filename)
except:
    print('file not found or cant open:')

for line in fhand:
    words = line.split()
    if len(words) < 3 or words[0] != 'From': continue
    else:
        if words[1] not in dictionary:
            dictionary[words[1]] = 1
        else:
            dictionary[words[1]] += 1

for k,v in list(dictionary.items()):
    list1.append((v,k))

list1.sort(reverse=True)

for k,v in list1[:1]:
    print(v,k)
