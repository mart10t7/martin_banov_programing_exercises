list1 = list()
dictionary = dict()
filename = input('enter file name: ')
try:
    fhand = open(filename)
except:
    print('file not found or cant open:')

for line in fhand:
    words = line.split()
    if len(words) < 3 or words[0] != 'From': continue
    hoursPosition = words[5].find(':')
    hours = words[5][:hoursPosition]
    if hours not in dictionary:
        dictionary[hours] = 1
    else:
        dictionary[hours] += 1

for k,v in list(dictionary.items()):
    list1.append((k,v))

list1.sort()

for k,v in list1:
    print(k,v)
