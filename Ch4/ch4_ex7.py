def computegrade(score):
 try: # lets you test a block of code for errors
     score=float(score) # convert score to float
     if score < 0.0 or score > 1.0: # if score is less than 0 or greater than 1
         return "Bad score"
     elif score >= 0.9:
         return "A"
     elif score >= 0.8:
         return "B"
     elif score >= 0.7:
         return "C"
     elif score >= 0.6:
         return "D"
     else:
         return "F"
 except: # lets you handle the error
     return "Bad score"
input_score = input("Enter score: ")
print(computegrade(input_score))