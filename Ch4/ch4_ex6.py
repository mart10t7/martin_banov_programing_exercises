def computepay(hours,rate):
 pay = 0
 hours = float(hours)
 rate = float(rate)
 if hours > 40:
     extra_hours = hours - 40
     pay = 40 * rate + 1.5 * rate * extra_hours
 else:
    pay = hours * rate
 return pay

input_hours = input("Enter hours:")
input_rate = input("Enter rate:")
pay = computepay(input_hours,input_rate)
print(pay)