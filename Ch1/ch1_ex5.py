# What is wrong with the following code:
>>> primt 'Hello world!'
File "<stdin>", line 1
primt 'Hello world!'
^
SyntaxError: invalid syntax
>>>
# The print keyword is written with 'm' instead of a 'n' and brackets are missing.