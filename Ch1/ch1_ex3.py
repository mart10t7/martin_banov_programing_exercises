# What is the difference between a compiler and an interpreter?
# Interpreter translates one argument at a time while the compiler scans the entire program before translating it to machine code.
# Interpreter takes less time to analyze code but more time to execute. Compiler does the opposite
# Programing languages using Interpreter Python, Ruby; Compiler C, C++